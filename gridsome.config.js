// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

process.env.GRIDSOME_DATE = "2019-10-08"
process.env.GRIDSOME_EVENT = "JAMstack AQ"

module.exports = {
  siteName: 'JAMstack AQ',
  plugins: [
    {
      use: 'gridsome-plugin-tailwindcss'
    },
    {
      use: 'gridsome-plugin-svg'
    },
    {
      use: 'gridsome-plugin-base-components'
    }
  ]
};
